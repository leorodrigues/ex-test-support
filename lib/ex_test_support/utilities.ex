defmodule ExTestSupport.Utilities do
  use GenServer

  @myself __MODULE__

  @type error :: {:error, reason :: atom}
  @type new_path :: {:new, path :: binary}
  @type existing_path :: {:existing, path :: binary}
  @type path_operation_result :: new_path | existing_path | error
  @type path_resoluton_result :: String.t | error

  def start_link(opts \\ [ ]) do
    GenServer.start_link(__MODULE__, opts, Keyword.merge([name: @myself], opts))
  end

  @impl true
  def init(arguments) do
    {:ok, Enum.into(arguments, %{
      envs: %{ },
      session_id: make_time_based_cipher()
    })}
  end

  @spec make_time_based_cipher :: binary
  def make_time_based_cipher do
    time_to_cipher(DateTime.now("Etc/UTC"))
  end

  @spec setup_environment(target_app_name :: atom) :: any
  def setup_environment(target_app_name) do
    GenServer.call(@myself, {:setup_environment, target_app_name})
  end

  @spec make_session_dir(
    target_app_name :: atom,
    path_parts :: [String.t]
  ) :: path_operation_result
  def make_session_dir(target_app_name, path_parts \\ [ ]) do
    GenServer.call(@myself, {:make_session_dir, target_app_name, path_parts})
  end

  @spec make_session_resources_dir(
    target_app_name :: atom,
    path_parts :: [String.t]
  ) :: path_operation_result
  def make_session_resources_dir(target_app_name, path_parts \\ [ ]) do
    GenServer.call(@myself, {:make_session_resources_dir, target_app_name, path_parts})
  end

  @spec resolve_session_dir(
    target_app_name :: atom,
    path_parts :: [String.t]
  ) :: path_resoluton_result
  def resolve_session_dir(target_app_name, path_parts \\ [ ]) do
    GenServer.call(@myself, {:resolve_session_dir, target_app_name, path_parts})
  end

  @spec resolve_resource_path(
    target_app_name :: atom,
    sub_path :: [String.t]
  ) :: path_resoluton_result
  def resolve_resource_path(target_app_name, sub_path \\ [ ]) do
    GenServer.call(@myself, {:resolve_resource_path, target_app_name, sub_path})
  end

  @impl true
  def handle_call({:setup_environment, target_app_name}, _from, state) do
    {:reply, :ok, handle_setup_environment(target_app_name, state)}
  end

  @impl true
  def handle_call({:make_session_dir, target_app_name, path_parts}, _from, state) do
    {:reply, handle_make_session_dir(target_app_name, path_parts, state), state}
  end

  @impl true
  def handle_call({:make_session_resources_dir, target_app_name, path_parts}, _from, state) do
    {:reply, handle_make_session_resources_dir(target_app_name, path_parts, state), state}
  end

  @impl true
  def handle_call({:resolve_session_dir, target_app_name, path_parts}, _from, state) do
    {:reply, handle_resolve_session_dir(target_app_name, path_parts, state), state}
  end

  @impl true
  def handle_call({:resolve_resource_path, target_app_name, sub_path}, _from, state) do
    {:reply, handle_resolve_resource_path(target_app_name, sub_path, state), state}
  end

  defp handle_setup_environment(target_app_name, %{envs: e} = state) do
    if Map.has_key?(e, target_app_name) do
      state
    else
      target_app_name
        |> configure_app_paths(state)
        |> try_copying_existing_resources(target_app_name)
    end
  end

  defp handle_resolve_resource_path(target_app_name, sub_path, %{envs: e}) do
    case Map.get(e, target_app_name) do
      nil -> {:error, :env_not_setup}
      %{session_resources_dir: d} -> Path.join([d|sub_path])
    end
  end

  defp handle_resolve_session_dir(target_app_name, path_parts, %{envs: e}) do
    case Map.get(e, target_app_name) do
      nil -> {:error, :env_not_setup}
      %{session_dir: s} -> Path.join([s|path_parts])
    end
  end

  defp handle_make_session_dir(target_app_name, path_parts, state) do
    case handle_resolve_session_dir(target_app_name, path_parts, state) do
      {:error, :env_not_setup} = e -> e
      path -> try_making_path(path)
    end
  end

  defp handle_make_session_resources_dir(target_app_name, path_parts, state) do
    case handle_resolve_resource_path(target_app_name, path_parts, state) do
      {:error, :env_not_setup} = e -> e
      path -> try_making_path(path)
    end
  end

  defp try_copying_existing_resources(%{envs: e} = state, target_app_name) do
    try_copying_existing_resources(Map.get(e, target_app_name))
    state
  end

  defp try_copying_existing_resources(%{
    base_resources_dir: s, session_resources_dir: d, session_dir: b
  }) do
    if File.exists?(s) do
      File.mkdir_p!(b)
      File.cp_r!(s, d)
    end
  end

  defp try_making_path(path) do
    case File.exists?(path) do
      false -> File.mkdir_p!(path); {:new, path}
      true -> {:existing, path}
    end
  end

  defp configure_app_paths(target_app_name, state) do
    with %{envs: e, session_id: id} <- state do
      app_dir = Application.app_dir(target_app_name)

      session_name = "test_session_#{id}"

      session_dir = Path.join([app_dir, session_name])
      session_resources_dir = Path.join([session_dir, "resources"])
      base_resources_dir = Path.join([
        compute_path(target_app_name), "test", "resources"
      ])

      Map.put(state, :envs, Map.put(e, target_app_name, %{
        session_id: id,
        session_dir: session_dir,
        session_name: session_name,
        base_resources_dir: base_resources_dir,
        session_resources_dir: session_resources_dir
      }))
    end
  end

  defp time_to_cipher({:ok, time}) do
    '#{DateTime.to_unix(time)}'
      |> Enum.map(fn c -> c + 49 end)
      |> List.to_string()
  end

  defp compute_path(target_app_name) do
    compute_path(to_string(target_app_name), File.cwd!(), under_umbrella())
  end

  defp compute_path(_, current_dir, false), do: current_dir
  defp compute_path(target_app_name, current_dir, true) do
    case String.ends_with?(current_dir, Path.join("apps", target_app_name)) do
      true -> current_dir
      false -> Path.join([current_dir, "apps", target_app_name])
    end
  end

  defp under_umbrella do
    Application.get_env(:ex_test_support, :under_umbrella, false)
  end
end
