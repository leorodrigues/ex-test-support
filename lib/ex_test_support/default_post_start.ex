defmodule ExTestSupport.DefaultPostStart do

  require Logger

  def invoke(%{post_start_modules: modules} = context) do
    Logger.info([msg: "Running post start modules", modules: modules])
    context
  end
end
