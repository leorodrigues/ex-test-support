defmodule ExTestSupport.EnvProviderMock do

  @default_mf {Application, :get_env}
  @key :mock_for_get_env

  def get_env(app_name, key, default \\ nil) do
    with {module, function} <- Keyword.get(opts(), @key, @default_mf) do
      apply(module, function, [app_name, key, default])
    end
  end

  defp opts do
    Application.get_env(:ex_test_support, __MODULE__, [ ])
  end
end
