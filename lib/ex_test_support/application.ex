defmodule ExTestSupport.Application do
  @moduledoc false

  require Logger
  use Application

  @post_start [ExTestSupport.DefaultPostStart]
  @app_name :ex_test_support

  @impl true
  def start(_type, _args) do
    Logger.info("Starting test support")

    children = [
      {ExTestSupport.Utilities, [ ]}
    ]

    opts = [strategy: :one_for_one, name: ExTestSupport.Supervisor]
    result = Supervisor.start_link(children, opts)
    fire_up_post_start_modules()
    result
  end

  defp fire_up_post_start_modules do
    post_start_modules = get_post_start_modules()
    context = %{post_start_modules: post_start_modules}
    fire_up_post_start_modules(post_start_modules, context)
  end

  defp fire_up_post_start_modules([ ], context), do: context

  defp fire_up_post_start_modules([head|tail], context) do
    fire_up_post_start_modules(tail, apply(head, :invoke, [context]))
  end

  defp get_post_start_modules do
    @post_start ++ Application.get_env(@app_name, :post_start_modules, [ ])
  end
end
