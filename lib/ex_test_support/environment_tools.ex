defmodule ExTestSupport.EnvironmentTools do
  alias ExTestSupport.Utilities, as: Utils

  defmacro __using__(opts) do
    unless :target_app_name in Keyword.keys(opts) do
      raise ArgumentError, "The :target_app_name parameter is required."
    end

    quote location: :keep, bind_quoted: [opts: opts] do
      use ExUnit.Case

      import ExTestSupport.EnvironmentTools

      @target_app_name Keyword.get(opts, :target_app_name)

      @spec make_time_based_cipher :: String.t
      defp make_time_based_cipher do
        Utils.make_time_based_cipher
      end

      @spec make_session_dir(path_parts :: [String.t]) :: Utils.path_operation_result
      defp make_session_dir(path_parts \\ [ ]) do
        Utils.make_session_dir(@target_app_name, path_parts)
      end

      @spec make_session_resources_dir(path_parts :: [String.t]) :: Utils.path_operation_result
      defp make_session_resources_dir(path_parts \\ [ ]) do
        Utils.make_session_resources_dir(@target_app_name, path_parts)
      end

      @spec resolve_session_dir(path_parts :: [String.t]) ::  Utils.path_resolution_result
      defp resolve_session_dir(path_parts \\ [ ]) do
        Utils.resolve_session_dir(@target_app_name, path_parts)
      end

      @spec resolve_resource_path(sub_path :: [String.t]) :: Utils.path_resolution_result
      defp resolve_resource_path(sub_path \\ [ ]) do
        Utils.resolve_resource_path(@target_app_name, sub_path)
      end

      @spec resolve_test_resource_path(sub_path :: [String.t]) :: Utils.path_resolution_result
      defp resolve_test_resource_path(sub_path \\ [ ]) do
        Utils.resolve_resource_path(@target_app_name, test_sub_path() ++ sub_path)
      end

      @spec test_sub_path() :: String.t
      defp test_sub_path, do: String.split(Macro.underscore(__MODULE__), "/")
    end
  end

  defmacro setup_all_environment(do: block) do
    quote do
      setup_all do
        Utils.setup_environment(@target_app_name)
        unquote(block)
      end
    end
  end
end
