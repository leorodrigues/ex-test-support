defmodule ExTestSupport.EnvironmentToolsTest do
  use ExTestSupport.EnvironmentTools, target_app_name: :ex_test_support

  require Logger

  setup_all_environment do
    Logger.info("Starting test support tests")
    :ok
  end

  test "Should make session dir" do
    {:new, path} = make_session_resources_dir()
    true = File.exists?(path)

    ["sub_path", "environment_tools_test", "ex_test_support"|_] = Enum.reverse(
      Path.split(resolve_test_resource_path(["sub_path"]))
    )
  end
end
