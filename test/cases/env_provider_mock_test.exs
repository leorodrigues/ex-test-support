defmodule ExTestSupport.EnvProviderMockTest do
  use ExUnit.Case

  alias ExTestSupport.EnvProviderMock

  setup do
    :meck.new(FakeModule, [:non_strict])
    on_exit(fn ->
      :meck.unload()
    end)
  end

  describe "ExTestSupport.EnvProviderMock.get_env" do
    test "should use the given mock" do
      Application.put_env(:ex_test_support, EnvProviderMock, [
        mock_for_get_env: {FakeModule, :handle_it}
      ])

      :meck.expect(FakeModule, :handle_it, fn _, _, _ -> 112358 end)

      112358 = EnvProviderMock.get_env(:a, :b, :c)

      :meck.wait(FakeModule, :handle_it, [:a, :b, :c], 1000)

      Application.put_env(:ex_test_support, EnvProviderMock, [ ])
    end

    test "should fall back to Application.get_env" do
      Application.put_env(:fake_app_a, :fake_key_a, "x")

      "x" = EnvProviderMock.get_env(:fake_app_a, :fake_key_a, "y")

      "y" = EnvProviderMock.get_env(:fake_app_a, :fake_key_b, "y")

      nil = EnvProviderMock.get_env(:fake_app_a, :fake_key_b)

      "z" = EnvProviderMock.get_env(:fake_app_b, :fake_key, "z")

      nil = EnvProviderMock.get_env(:fake_app_b, :fake_key)
    end
  end
end
