defmodule ExTestSupport.MixProject do
  use Mix.Project

  def project do
    [
      app: :ex_test_support,
      version: "2.0.0",
      elixir: "~> 1.12",
      elixirc_paths: elixirc_paths(Mix.env()),
      start_permanent: Mix.env() == :prod,
      deps: deps()
    ]
  end

  # Run "mix help compile.app" to learn about applications.
  def application do
    [
      extra_applications: [:logger],
      mod: {ExTestSupport.Application, []}
    ]
  end

  defp elixirc_paths(:test), do: ["lib", "test/support", "test/mockery"]
  defp elixirc_paths(:dev), do: ["lib", "test/mockery"]
  defp elixirc_paths(_), do: ["lib"]

  # Run "mix help deps" to learn about dependencies.
  defp deps do
    [
      {:dialyxir, "~> 0.5", only: [:dev], runtime: false},
      {:meck, "~> 0.9.2", only: [:test], runtime: false}
    ]
  end
end
